# Gitlab tools

## gitlab-env: source gitlab environment variables in your shell.

Run this tool as `. gitlab-env` inside your repository directory to
set all environment variables defined in gitlab Settings -> CI/CD in
your shell. You need to make sure you have a private token in your git
configuration. You can set it with:

```Shell
git config --global gitlab.token "<your private token>"
```

Obtain the token from your account settings in gitlab.

The project's URL is derived from the result of the `git remote
get-url origin` command. So the remote of the origin needs to be your
repository in gitlab.
